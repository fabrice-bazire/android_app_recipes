package com.dbordel.my_food_app.ActivitiesIngredient;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.TextView;

import com.dbordel.my_food_app.model.Ingredient;
import com.dbordel.my_food_app.R;

public class IngredientDetailActivity extends AppCompatActivity {

    TextView id, nom;

    Ingredient ingredient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ingredient_activity_detail);

        nom = findViewById(R.id.IngredientNom);

        if(getIntent().hasExtra("ingredient_selected")){
            Ingredient ingredient = getIntent().getParcelableExtra("ingredient_selected");
            nom.setText(ingredient.getNom());

            Log.d("meslogs", "ingredient.getnom " +  ingredient.toString());
        }
    }

    public void retourListeIngredient(View v){
        finish();
    }

}
