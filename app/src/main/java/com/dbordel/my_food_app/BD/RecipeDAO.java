package com.dbordel.my_food_app.BD;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dbordel.my_food_app.model.Ingredient;
import com.dbordel.my_food_app.model.Recipe;

import java.util.List;

@Dao
public interface RecipeDAO {

    @Insert
    void insert(Recipe recipe);

    @Update
    void update(Recipe recipe);

    @Delete
    void delete(Recipe recipe);

    @Query("DELETE From recette")
    void deleteAll();

    @Query("Select * from recette")
    List<Recipe> getRecipes();

    @Query("Select * from recette")
    LiveData<List<Recipe>> getRecipeLD();

    @Query("Select * from recette where IngredientId=:id")
    List<Recipe> getRecetteByIngredient(int id);
}


