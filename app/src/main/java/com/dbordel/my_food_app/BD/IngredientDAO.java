package com.dbordel.my_food_app.BD;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dbordel.my_food_app.model.Ingredient;

import java.util.List;


@Dao
public interface IngredientDAO {

    @Insert
    Long insertIngredient(Ingredient ingredient);

    @Update
    void updateIngredient(Ingredient ingredient);

    @Delete
    void deleteIngredient(Ingredient ingredient);

    @Query("DELETE From Ingredient")
    void deleteAll();

    @Query("Select * from Ingredient")
    List<Ingredient> getIngredients();

    //LiveDATA
    @Query("Select * from Ingredient")
    LiveData<List<Ingredient>> getIngredientsLD();

    @Query("Select count(*) from Ingredient")
    int count();

    @Query("SELECT count(*) from Ingredient")
    LiveData<Integer> countLD();
}
