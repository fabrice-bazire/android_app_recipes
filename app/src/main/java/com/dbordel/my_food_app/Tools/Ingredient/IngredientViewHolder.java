package com.dbordel.my_food_app.Tools.Ingredient;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.Tools.ItemClickListener;

//Un ViewHolder permet de placer les objets dans les cellules de la RecyclerView
public class IngredientViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView nom;
    Button iconeShow,iconeDelete,iconeEdit;

    ItemClickListener itemClickListener;

    public IngredientViewHolder(@NonNull View itemView, ItemClickListener itemClickListener) {
        super(itemView);
        nom = itemView.findViewById(R.id.cardViewNomIngredient);
        iconeShow = itemView.findViewById(R.id.buttonShowIngredient);
        iconeDelete = itemView.findViewById(R.id.buttonDeleteIngredient);
        iconeEdit = itemView.findViewById(R.id.buttonUpdateIngredient);

        this.itemClickListener = itemClickListener;
        //itemView.setOnClickListener(this);
        iconeShow.setOnClickListener(this);
        iconeDelete.setOnClickListener(this);
        iconeEdit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.buttonShowIngredient:
                itemClickListener.onShowClick(getAdapterPosition());
                break;

            case R.id.buttonUpdateIngredient:
                itemClickListener.onUpdateClick(getAdapterPosition());
                break;

            case R.id.buttonDeleteIngredient:
                itemClickListener.onDeleteClick(getAdapterPosition());
                break;

            default:
                break;
        }

    }

}


