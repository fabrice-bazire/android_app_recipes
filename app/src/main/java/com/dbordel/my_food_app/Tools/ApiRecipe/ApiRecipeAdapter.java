package com.dbordel.my_food_app.Tools.ApiRecipe;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.my_food_app.DownloadImageTask;
import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.model.ApiRecipe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApiRecipeAdapter extends RecyclerView.Adapter<ApiRecipeAdapter.ViewHolder>{
    private List<ApiRecipe> mlistItems;
    private Context mContext;
    private static Map<String,Bitmap> bitmaps = new HashMap<>();

    public ApiRecipeAdapter(List<ApiRecipe> listItems, Context context){
        this.mlistItems = listItems;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        //Inflate the custom layout
        View view = inflater.inflate(R.layout.card_api_recipe_model,parent,false);
        //Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ApiRecipe recipe = mlistItems.get(position);
        holder.display(recipe);
    }

    @Override
    public int getItemCount() {
        return mlistItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mTextViewTitle;
        private ImageView mImageView;
        private TextView mTextViewPublisher;
        private TextView mTextViewSource;
        private NumberPicker mNumberSocialRange;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mTextViewTitle = (TextView) itemView.findViewById(R.id.recipe_list_title);
            mTextViewPublisher = (TextView) itemView.findViewById(R.id.recipe_list_publisher);
            mTextViewSource = (TextView) itemView.findViewById(R.id.recipe_source_url);
            //mNumberSocialRange = (NumberPicker) itemView.findViewById(R.id.recipe_list_social_rank);
            mImageView = (ImageView) itemView.findViewById(R.id.recipe_list_image);

        }

        public void display(ApiRecipe recipe) {
            mTextViewTitle.setText(recipe.getTitle());
            mTextViewPublisher.setText(recipe.getPublisher());
            mTextViewSource.setText(recipe.getSourceUrl());
            //holder.mNumberSocialRange.setValue(recipe.getSocialRank());
            if (bitmaps.containsKey(recipe.getImageUrl())){
                mImageView.setImageBitmap(bitmaps.get(recipe.getImageUrl()));
            } else {
                new DownloadImageTask(mImageView).execute(recipe.getImageUrl());
            }
        }
    }
}
