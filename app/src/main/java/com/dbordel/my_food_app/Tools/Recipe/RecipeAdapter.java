package com.dbordel.my_food_app.Tools.Recipe;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.my_food_app.model.Recipe;
import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.Tools.ItemClickListener;

import java.util.List;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeViewHolder> implements View.OnClickListener {

    List<Recipe> lesRecettes;
    private ItemClickListener mItemClickListener;

    public RecipeAdapter(List<Recipe> listRecette, ItemClickListener itemClickListener){
        this.lesRecettes = listRecette;
        this.mItemClickListener = itemClickListener;
    }



    //Create new views for RecyclerView (invoked by the layoutManager)
    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int ViewType){
        // create a new view
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.card_myrecipe_model,parent,false);
        RecipeViewHolder recipeViewHolder = new RecipeViewHolder(view,mItemClickListener);
        return recipeViewHolder;


    }


    //Replace the contents of a view (invoked by the layoutManager)
    @Override
    public void onBindViewHolder(@NonNull final RecipeViewHolder recipeViewHolder, final int position) {
        //get element from the dataset at this position
        //replace the contents of the view with that element
        final Recipe recipe = lesRecettes.get(position);
        recipeViewHolder.nom.setText(recipe.getNom());

    }



    @Override
    public int getItemCount() {
        return lesRecettes.size();
    }

    @Override
    public void onClick(View v) {
        Log.d("mesLogs", "onclick");
    }

    //utile pour la suppression
    public Recipe getLaRecette(int position){
        return  lesRecettes.get(position);

    }
}





