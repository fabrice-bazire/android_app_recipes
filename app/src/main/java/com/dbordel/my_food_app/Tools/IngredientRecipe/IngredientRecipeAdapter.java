package com.dbordel.my_food_app.Tools.IngredientRecipe;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.model.IngredientRecipe;
import com.dbordel.my_food_app.model.Recipe;

import java.util.List;

public class IngredientRecipeAdapter extends RecyclerView.Adapter<IngredientRecipeViewHolder> {
    private List<Recipe> lesrecettes;

    public IngredientRecipeAdapter(List<Recipe> lesrecettes) {
        this.lesrecettes = lesrecettes;
    }


    @NonNull
    public IngredientRecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ingredient_item_layout_menu,parent,false);
        return new IngredientRecipeViewHolder(view);
    }

    public void onBindViewHolder(@NonNull IngredientRecipeViewHolder holder, int position) {
        final Recipe recette = lesrecettes.get(position);
        //holder.famille.setText(plante.getFamilleId());
        holder.nom.setText(recette.getNom());
    }

    public int getItemCount() {
        return lesrecettes.size();
    }
}
