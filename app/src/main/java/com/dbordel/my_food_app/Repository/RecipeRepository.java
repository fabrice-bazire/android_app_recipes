package  com.dbordel.my_food_app.Repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.dbordel.my_food_app.BD.AppDatabase;
import com.dbordel.my_food_app.BD.RecipeDAO;
import com.dbordel.my_food_app.model.Recipe;

import java.util.List;

public class RecipeRepository {

    private RecipeDAO recipeDAO;
    private LiveData<List<Recipe>> recipeLD;
    private LiveData<String> nameRecipe;

    public RecipeRepository(Application application) {
        AppDatabase appDatabase = AppDatabase.getDatabaseInstance(application);
        recipeDAO = appDatabase.getRecipeDAO();
        recipeLD = recipeDAO.getRecipeLD();
    }

    public void insert(Recipe recipe) {
        new InsertAsyncTask(recipeDAO).execute(recipe);
    }

    LiveData<List<Recipe>> getAllRecipes() {
        return recipeLD;
    }

    public void update(Recipe recipe) {
        new UpdateAsyncTask(recipeDAO).execute(recipe);
    }

    public void delete(Recipe recipe) {
        new DeleteAsyncTask(recipeDAO).execute(recipe);
    }

    public void deleteAll() {
        new RecipeRepository.DeleteAllAsync(recipeDAO).execute();
    }

    private static class DeleteAllAsync extends AsyncTask<Void, Void, Void> {
        private RecipeDAO recipeDAO;

        public DeleteAllAsync(RecipeDAO t) {
            recipeDAO = t;
        }

        @Override
        protected Void doInBackground(Void... args) {
            recipeDAO.deleteAll();
            return null;
        }
    }


    private class OperationsAsyncTask extends AsyncTask<Recipe, Void, Void> {

        RecipeDAO mAsyncTaskDao;

        OperationsAsyncTask(RecipeDAO dao) {
            this.mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Recipe... recipes) {
            return null;
        }
    }

    private class InsertAsyncTask extends OperationsAsyncTask {

        InsertAsyncTask(RecipeDAO mRecipeDao) {
            super(mRecipeDao);
        }

        @Override
        protected Void doInBackground(Recipe... recipes) {
            mAsyncTaskDao.insert(recipes[0]);
            return null;
        }
    }

    private class UpdateAsyncTask extends OperationsAsyncTask {

        UpdateAsyncTask(RecipeDAO recipeDAO) {
            super(recipeDAO);
        }

        @Override
        protected Void doInBackground(Recipe... recipes) {
            mAsyncTaskDao.update(recipes[0]);
            return null;
        }
    }

    private class DeleteAsyncTask extends OperationsAsyncTask {

        public DeleteAsyncTask(RecipeDAO recipeDAO) {
            super(recipeDAO);
        }

        @Override
        protected Void doInBackground(Recipe... recipes) {
            mAsyncTaskDao.delete(recipes[0]);
            return null;
        }
    }

    public LiveData<List<Recipe>> getRecipeLD(){return recipeLD;}

    public List<Recipe> getRecipes(){
        try{
            return new GetTasksAsync(recipeDAO).execute().get();
        }catch (Exception e){
            Log.d("MesLogs", "pb repository getRecipes");
        }
        return null;
    }

    private static class GetTasksAsync extends AsyncTask<Void, Void, List<Recipe>> {
        private RecipeDAO recipeDAO;

        public GetTasksAsync (RecipeDAO t){
            recipeDAO = t;
        }

        @Override
        protected List<Recipe> doInBackground(Void ... args){
            return recipeDAO.getRecipes();
        }
    }

}


