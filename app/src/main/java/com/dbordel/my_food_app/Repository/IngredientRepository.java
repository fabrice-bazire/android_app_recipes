package com.dbordel.my_food_app.Repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.dbordel.my_food_app.BD.AppDatabase;
import com.dbordel.my_food_app.BD.IngredientDAO;
import com.dbordel.my_food_app.model.Ingredient;

import java.util.List;

public class IngredientRepository {

    private IngredientDAO ingredientDAO;
    private LiveData<List<Ingredient>> ingredientLD;
    private LiveData<Integer> nbIngredientsLD;

    public IngredientRepository(Application application){
        AppDatabase appDatabase = AppDatabase.getDatabaseInstance(application);
        ingredientDAO = appDatabase.getIngredientDAO();
        ingredientLD = ingredientDAO.getIngredientsLD();
        nbIngredientsLD = ingredientDAO.countLD();
    }

// --------------------------- ASYNTASKS METHODES CRUD -------------------------------------------------------

    //voir doc https://developer.android.com/reference/android/os/AsyncTask

    //------------------------- INSERT ingredient ----------------------

    public Long insert (Ingredient ingredient){
        try {
            return new InsertAsyncTask(ingredientDAO).execute(ingredient).get();
        }catch (Exception e){
            Log.d("MesLogs", "pb Insertion ingredient");
            return null;
        }
    }

    protected static class InsertAsyncTask extends AsyncTask<Ingredient, Void, Long> {
        private IngredientDAO ingredientDao;

        public InsertAsyncTask (IngredientDAO p){
            ingredientDao = p;
        }

        @Override
        protected Long doInBackground(Ingredient... ingredients){
            return ingredientDao.insertIngredient(ingredients[0]);
        }
    }

//-------------------------------- UPDATE -----------------------------------

    public void update (final Ingredient ingredient){
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                ingredientDAO.updateIngredient(ingredient);
            }
        });
    }

//--------------------------------- DELETE ------------------------------------

    public void delete (final Ingredient ingredient){
        new DeleteAsync(ingredientDAO).execute(ingredient);
    }

    private static class DeleteAsync extends AsyncTask<Ingredient, Void, Void>{
        private IngredientDAO ingredientDAO;

        public DeleteAsync (IngredientDAO t){
            ingredientDAO = t;
        }

        @Override
        protected Void doInBackground(Ingredient... ingredients){
            ingredientDAO.deleteIngredient(ingredients[0]);
            return null;
        }
    }

// -------------------------- DELETE ALL ---------------------------------------------

    public void deleteAll (){
        new DeleteAllAsync(ingredientDAO).execute();
    }

    private static class DeleteAllAsync extends AsyncTask<Void, Void, Void>{
        private IngredientDAO ingredientDAO;

        public DeleteAllAsync (IngredientDAO t){
            ingredientDAO = t;
        }

        @Override
        protected Void doInBackground(Void ... args){
            ingredientDAO.deleteAll();
            return null;
        }
    }

//----------------------------------- COUNT ----------------------------------------------------

    public Integer count (){
        try {
            return new CountAsync(ingredientDAO).execute().get();
        }catch (Exception e){
            Log.d("MesLogs", "pb count");
        }
        return null;
    }

    private static class CountAsync extends AsyncTask<Void, Void, Integer>{
        private IngredientDAO ingredientDAO;

        public CountAsync (IngredientDAO t){
            ingredientDAO = t;
        }

        @Override
        protected Integer doInBackground(Void ... args){
            return ingredientDAO.count();
        }
    }


    // -------------------------- FIN METHODE ASYNTASK -----------------------------------------------


    //LIVEDATA


    public LiveData<List<Ingredient>> getIngredientLD() {
        return ingredientLD;
    }

    public LiveData<Integer> getNbIngredientsLD() {
        return nbIngredientsLD;
    }






}


