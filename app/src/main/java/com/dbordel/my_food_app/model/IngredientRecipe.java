package com.dbordel.my_food_app.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class IngredientRecipe {
    @Embedded
    private Ingredient ingredient;

    @Relation(
            parentColumn = "id_ingredient",
            entityColumn = "IngredientID"
    )

    public List<Recipe> recipeList;

    public IngredientRecipe() {
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public List<Recipe> getRecipeList() { return recipeList; }

    public void setRecipeList(List<Recipe> recipeList) {
        this.recipeList= recipeList;
    }
}
