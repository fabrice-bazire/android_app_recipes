package com.dbordel.my_food_app.ActivitiesRecipe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.dbordel.my_food_app.ActivitiesIngredient.IngredientMainActivity;
import com.dbordel.my_food_app.BD.AppDatabase;
import com.dbordel.my_food_app.BD.RecipeDAO;

import com.dbordel.my_food_app.MainActivity;
import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.Tools.Recipe.RecipeAdapter;
import com.dbordel.my_food_app.Tools.Recipe.RecipeViewModel;
import com.dbordel.my_food_app.Tools.ItemClickListener;

public class RecipeListActivity extends AppCompatActivity implements ItemClickListener {

    RecyclerView recyclerView;
    RecipeDAO recipeDAO;
    RecipeViewModel recetteViewModel;
    TextView noRecipeMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recette_activity_liste);

        recyclerView = findViewById(R.id.recetteRecyclerView);
        recipeDAO = AppDatabase.getDatabaseInstance(this).getRecipeDAO();
        Log.d("recipes list",recipeDAO.getRecipes().toString());
        if (recipeDAO.getRecipes().size()==0){
            noRecipeMessage = findViewById(R.id.any_recipe);
            noRecipeMessage.setText("Aucune recette créée");
        }
        RecipeAdapter recipeAdapter = new RecipeAdapter(recipeDAO.getRecipes(),this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recipeAdapter);
    }


    @Override
    public void onShowClick(int position) {
        Log.d("meslogs", "recette test bouton cliqué show  " +position);
        Intent intent = new Intent(this, RecipeDetailActivity.class);
        intent.putExtra("recette_selected", recipeDAO.getRecipes().get(position));
        startActivity(intent);
    }
    @Override
    public void onUpdateClick(int position) {
        Log.d("meslogs", "recette test bouton cliqué update  "+ position);
        Intent intent = new Intent(this, RecipeUpdateActivity.class);
        intent.putExtra("updateRecette", recipeDAO.getRecipes().get(position));
        startActivity(intent);
    }


    @Override
    public void onDeleteClick(int position) {
        Log.d("meslogs", "recette test bouton cliqué delete  " + position );
        RecipeAdapter recipeAdapter = new RecipeAdapter(recipeDAO.getRecipes(),this);
        recipeDAO.delete(recipeAdapter.getLaRecette(position));
        recreate();
    }

    public void addRecipe(View view){
        Log.d("in add recipe", "enter");
        Intent intent = new Intent(this, RecipeMainActivity.class);
        startActivity(intent);
    }

    /*public void returnHome(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }*/

    /*public static class MyRecipesActivity extends AppCompatActivity {
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.my_recipes_activity);
        }

        public void recetteLink(View view){
            Intent intent = new Intent(this, RecipeMainActivity.class);
            startActivity(intent);
        }

        public void ingredientLink(View view){
            Intent intent = new Intent(this, IngredientMainActivity.class);
            startActivity(intent);
        }
    }*/
}

