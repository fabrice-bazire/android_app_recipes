package com.dbordel.my_food_app.ActivitiesRecipe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.dbordel.my_food_app.BD.AppDatabase;
import com.dbordel.my_food_app.BD.RecipeDAO;
import com.dbordel.my_food_app.R;
import com.dbordel.my_food_app.model.Recipe;

public class RecipeUpdateActivity extends AppCompatActivity {

    private EditText nom, preparation;

    private RecipeDAO mRecipeDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recette_edit);

        nom = findViewById(R.id.editTextRecette);
        preparation = findViewById(R.id.editTextPreparation);

        if(getIntent().hasExtra("updateRecette")){
            Recipe recipe = getIntent().getParcelableExtra("updateRecette");
            nom.setText(recipe.getNom());
            preparation.setText(recipe.getPreparation());
            Log.d("meslogs", "OnCreate : " + recipe.toString());
        }
        mRecipeDAO = AppDatabase.getDatabaseInstance(this).getRecipeDAO();

    }

    public void saveRecette(View view){

        Recipe recipe = getIntent().getParcelableExtra("updateRecette");
        recipe.setNom(nom.getText().toString());
        recipe.setPreparation(preparation.getText().toString());

        mRecipeDAO.update(recipe);
        Intent intent = new Intent(this, RecipeListActivity.class);

        Toast.makeText(this, "Recette modifié !", Toast.LENGTH_SHORT).show();
        startActivity(intent);

    }

    public void showRecette(View view){
        finish();
    }
}